const jsonwebtoken = require('jsonwebtoken');
require('dotenv').config({path: 'src/.env'});
const User = require('../models/User');


const signUp = async (req, res,next)=>{
    const {_name, _email, _pass} = req.body;
    const userI = new User({
        user: _name,
        email: _email,
        password: _pass
    });
    
    userI.password = await userI.crypcon(userI.password);
    await userI.save();

    const token =jsonwebtoken.sign({id: userI._id }, process.env.SECRET,{
        expiresIn: 60 * 60 * 24
    });

    res.json({auth:true, token});
    
};


const signIn = async (req, res,next)=>{
    const {email, password} = req.body;
   const user = await User.findOne({email});

   if (!user) {
       return res.status(404).send("dosent exist");
   }
 const pass = await user.verify(password);

 if (!pass) {
    return res.status(404).send("token null");
 }
 const token =jsonwebtoken.sign({id: user._id }, process.env.SECRET,{
    expiresIn: 60 * 60 * 24
});

res.json({auth:true, token});

 
};

const inside = async(req, res,next)=>{
    
   const user = await User.findById(req.userId, {password: 0});

   if (!user) {
    res.status(404).json({
        mes: 'not found user'
    })
   }
   res.status(200).json({user});

   console.log(decode);
   
};

module.exports = {
    signUp,
    signIn,
    inside
}