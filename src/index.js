
require('dotenv').config({path: 'src/.env'});
const app = require('./app')
require('./database');

async function main(){
    app.set('port', process.env.PORT);

   await app.listen(app.get('port'), () =>{
        console.log(`PORT IN ${app.get('port')}`);
    });
}
main();