const {Schema, model} = require('mongoose');
const bcryptjs = require('bcryptjs');

const userSchema = new Schema({
    user: String,
    email: String,
    password: String
});

/** Extendiendo el metodo para encriptar la contraseña */
userSchema.methods.crypcon = async (password) =>{
    const salt = await bcryptjs.genSalt(8);
    return bcryptjs.hash(password, salt);
}

userSchema.methods.verify = function(password) {
   return bcryptjs.compare(password, this.password);
}
/** Accediendo a la bd, creando user y exportandolo*/
module.exports = model('User', userSchema);
