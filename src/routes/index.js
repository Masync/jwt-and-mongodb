const {Router} = require('express');
const controllerAuth = require('../controllers/auth.controller');
const routes = Router();
const verification = require('../controllers/verifyToken');
routes.post('/signUp',controllerAuth.signUp);
routes.post('/signIn',controllerAuth.signIn);
routes.get('/in',verification,controllerAuth.inside);

module.exports = routes;
